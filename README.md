<!--
SPDX-FileCopyrightText: 2023 David Runge <dvzrv@archlinux.org>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# archlinux.systemd_networkd

An [Ansible](https://www.ansible.com/) role to configure and enable [systemd-networkd](https://www.freedesktop.org/software/systemd/man/systemd-networkd.html).
The configurable components are [systemd.network](https://www.freedesktop.org/software/systemd/man/systemd.network.html), [systemd.link](https://www.freedesktop.org/software/systemd/man/systemd.link.html) and [systemd.netdev](https://www.freedesktop.org/software/systemd/man/systemd.netdev.html).

## Requirements

An Arch Linux system or an Arch Linux chroot.

## Variables

By default this role does nothing, as `systemd_networkd` is unset.

* `systemd_networkd`: a dict to define settings for systemd-networkd (unset by default)
  * `apply`: a boolean indicating whether to apply the configuration by restarting `systemd-networkd` (defaults to `false`)
  * `predictable_network_interface_names`: whether to use [PredictableNetworkInterfaceNames](https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames/) (defaults to `true`)
  * `links`: a dict of [systemd.link](https://www.freedesktop.org/software/systemd/man/systemd.link.html) configurations (defaults to `{}`)
  * `netdevs`: a dict of [systemd.netdev](https://www.freedesktop.org/software/systemd/man/systemd.netdev.html) configurations (defaults to `{}`)
  * `networks`: a dict of [systemd.network](https://www.freedesktop.org/software/systemd/man/systemd.network.html) configurations (defaults to `{}`)

Each key-value pair in `systemd_networkd.links`, `systemd_networkd.netdevs` and `systemd_networkd.netdevs` corresponds to respective files and their contents, named after the key (e.g. `20-ethernet` -> `20-ethernet.network`).
The values are expected to be lists of key-value pairs, in which the keys represent the section headers (e.g. `Match` -> `[Match]`) and the values lists of key-value pairs, which represent the entries in a section (e.g. `Name: eth0` -> `Name=eth0`).

The following variables have no defaults in this role, but are available to override behavior and use special functionality:

* `chroot_dir`: the directory to chroot into, when including `chroot.yml` tasks (defaults to unset)

## Dependencies

None

## Example Playbook

```yaml
- name: Configure a network profile and restart the systemd service
  hosts: my_hosts
  vars:
    systemd_networkd:
      apply: true
      networks:
        20-ethernet:
          - Match:
              - Name: eth0
          - Network:
              - DHCP: "no"
              - IPv6AcceptRouterAdvertisements: "no"
              - DNS: 8.8.8.8
              - DNS: 8.8.4.4
              - Domains: your.tld
              - Address: 192.0.2.176/24
              - Gateway: 192.0.2.1
  tasks:
    - name: Include archlinux.systemd_networkd
      ansible.builtin.include_role:
        name: archlinux.systemd_networkd
```

```yaml
- name: Configure a WireGuard network device in a chroot
  hosts: my_hosts
  vars:
    systemd_networkd:
      apply: true
      netdevs:
        99-wireguard:
          - NetDev:
              - Name: wg0
              - Kind: wireguard
              - Description: WireGuard tunnel wg0
          - WireGuard:
              - Listenport: 51222
              - PrivateKey: <MY_PRIVATE_KEY>
          - WireGuardPeer:
              - PublicKey: <MY_PUBLIC_KEY>
              - PresharedKey: <MY_PRESHARED_KEY>
              - AllowedIPs: 10.0.0.1/32
              - Endpoint: my.remote.host:51222
      networks:
        99-wireguard:
          - Match:
              - Name: wg0
          - Network:
              - Address: 10.0.0.3/24
  tasks:
    - name: Include archlinux.systemd_networkd
      ansible.builtin.include_role:
        name: archlinux.systemd_networkd
        tasks_from: chroot.yml
```

## License

GPL-3.0-or-later

## Author Information

David Runge <dvzrv@archlinux.org>
